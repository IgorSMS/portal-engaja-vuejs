import Vue from 'vue';
import Router from 'vue-router';
import Reports from './templates/Reports';
import HomePage from './templates/HomePage';
import TrackMissions from './templates/TrackMissions';
import TrackMissionsExtract from './templates/TrackMissionsExtract';
import TrackMissionsContentBelow from './templates/TrackMissionsContentBelow';
import Portfolio from './templates/Portfolio';
import Main from './templates/Main';
import EngajaMais from './templates/EngajaMais';


Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      name: 'inicio',
      path: '*',
      component: HomePage,
      meta: { transition: 'fade-in-down' },
    },
    {
      name: 'acompanhar-missoes',
      path: '/acompanhar-missoes',
      component: TrackMissions,
      children: [
        { path: 'extrato', component: TrackMissionsExtract, meta: { transition: 'fade-in-right' }},
        { path: 'missoes-extras', component: TrackMissionsContentBelow , meta: { transition: 'fade-in-right' }}
      ]
    },
    {
      name: 'relatorios',
      path: '/relatorios',
      component: Reports,
      meta: { transition: 'fade-in-down' },
      props: route => ({...route.params})
    },
    {
      name: 'programa',
      path: '/programa',
      component: Main,
      meta: { transition: 'fade-in-down' },
    },
    {
      name: 'engaja',
      path: '/engaja-mais',
      component: EngajaMais,
      meta: { transition: 'fade-in-down' },
    },
    {
      name: 'portfolio',
      path: '/portfolio',
      component: Portfolio,
      meta: { transition: 'fade-in-down' },
    }
  ]
});