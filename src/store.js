import Vue from 'vue'
import Vuex from 'vuex'
import * as serviceDataGame from './services/GameData.js'
import * as serviceEmployee from './services/ExtractOfUse.js'
import * as serviceExtractUnit from './services/ExtractUnit.js'
import * as serviceLogin from './services/Login.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userData: {},
    extractOfUse: [],
    extractUnit: [],
    dataLogin: {}
  },
  getters: {
    getUserData: state => state.userData,
    getUserPoints: state => {
      return {
        points: state.userData.iPontoAcumulado,
        level: state.userData.iNivelEmpregado
      }
    },
    getExtractUse: state => state.extractOfUse,
    getExtractUnit: state => state.extractUnit
  },
  mutations: {
    setUserData (state, data) {
      state.userData = data.filter(content => content.iMatricula == state.dataLogin.user.coMatricula)[0]
      state.userData.CheckMissao1 = state.userData["CheckMissao 1"];
      state.userData.CheckMissao2 = state.userData["CheckMissao 2"];
      state.userData.CheckMissao3 = state.userData["CheckMissao 3"];
      state.userData.CheckMissao4 = state.userData["CheckMissao 4"];
      state.userData.CheckMissao5 = state.userData["CheckMissao 5"];
      state.userData.CheckMissao6 = state.userData["CheckMissao 6"];
    },
    setextractOfUse (state, data) {
      state.extractOfUse = data
    },
    setExtractUnitState (state, data) {
      state.extractUnit = data
    },
    setDataLogin (state, data) {
      state.dataLogin = data
    },
  },
  actions: {
    async setUserServiceData ({ commit }) {
      //chamada para serviço

      let userData = {}
      await serviceDataGame.getDataGame().then(res => {
        userData = res;
      }).catch(error => {
        console.log(error)
        userData = require('@/assets/JSON/empregado_jogo.json');
      })

      commit('setUserData', userData)
    },
    async setExtractOfUseService ({ commit }) {
      //chamada para serviço
      let data
      await serviceEmployee.getExtractOfUse().then(res => {
        data = res;
      }).catch(error => {
        console.log(error)
        data = require('@/assets/JSON/extrato_empregado.json');
      })
      
      commit('setextractOfUse', data)
    },
    async setExtractUnit ({ commit }) {
      //chamada para serviço
      let ExtractUnit
      await serviceExtractUnit.getExtractUnit().then(res => {
        ExtractUnit = res;
      }).catch(error => {
        console.log(error)
        ExtractUnit = require('@/assets/JSON/extrato_unidade.json');
      })      
      commit('setExtractUnitState', ExtractUnit)
    },

    async setLoginData ({ commit }) {
      //chamada para serviço

      let dataLogin
      await serviceLogin.getLogin().then(res => {
        dataLogin = res;
      }).catch(error => {
        console.log(error)
        dataLogin = require('@/assets/JSON/login.json');
      })
      commit('setDataLogin', dataLogin)
    },
  }
})